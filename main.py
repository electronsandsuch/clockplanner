from tkinter import *
from tkcalendar import *
import simpleaudio as sa
import datetime
import time
import threading
from threading import Thread


class ClockPlanner(Tk):
        
    def __init__(self):
        self.cont = True
        self.setAlarm = False
        self.alarmOn = False
        self.alarmVal = ""

        self.win = Tk()
        self.win.title("Clock Planner")
        self.win.geometry("450x550")
        self.win.protocol("WM_DELETE_WINDOW", self.onExit)

        title = Label(self.win, text = "Clock Planner", font=("FreeSerif", 30))
        title.place(x = 0, y = 0)

        instructions = Label(self.win, text = "Set Alarm", font=("FreeSans", 12))
        instructions.place(x = 10, y = 60)

        self.hours = Entry(self.win)
        self.hours.place(x = 10, y = 80)

        self.minutes = Entry(self.win)
        self.minutes.place(x = 180, y = 80)

        # time of day selection
        self.val = StringVar(self.win)

        # time of day options
        self.tod = { 'AM','PM' }
        # default
        self.val.set('AM') 

        self.dropDown = OptionMenu(self.win, self.val, *self.tod)
        self.dropDown.place(x = 350, y = 80)

        self.alarm = Label(self.win, text="Alarm: None", fg = "red", font=("FreeSans", 25))
        self.alarm.place(x = 90, y = 120)

        self.clock = Label(self.win, text = "00:00:00", font=("FreeSans", 30))
        self.clock.place(x = 70, y = 260)

        d = datetime.datetime.now()
        #self.cal = Calendar(self.win, state = "disabled", selectmode = "day", year = d.year, month = d.month, day = d.day)
        self.cal = Calendar(self.win, selectmode = "day", year = d.year, month = d.month, day = d.day, date_pattern = "m/d/yyyy")
        self.cal.place(x = 70, y = 350)

        #print(self.cal.keys())

        button = Button(self.win, text = "Set", command = self.alarmSet)
        button.place(x = 150, y = 180)

    def alarmSet(self):
        self.setAlarm = True
        self.alarmVal = self.hours.get() + ":" + self.minutes.get() + ":00 " + self.val.get()
        if (self.alarmVal[0] == '0'):
            self.alarmVal = self.alarmVal[1:]
        self.alarm.config(text = "Alarm: " + self.alarmVal)

    def onExit(self):
        self.cont = False
        self.win.destroy()

    def mainloop(self):
        self.win.mainloop()

    def configClock(self, t):
        self.clock.config(text = t)

    def configCalendar(self, d):
        dstr = d.strftime("%-m/%-d/%Y")
        curr_d = self.cal.get_date()
        #gets a tuple of displayed month and year
        mon_yr = self.cal.get_displayed_month()
        #if datetime now is different from the current date set in calendar then change calendar date
        #need to check the displayed month/yr because otherwise only fixes date when change selected day
        #not when change selected month or year
        if (dstr != curr_d or mon_yr[0] != d.month or mon_yr[1] != d.year):
            self.cal.selection_set(d)

    def doCont(self):
        return self.cont

    def getSetAlarm(self):
        return self.setAlarm

    def getAlarmVal(self):
        return self.alarmVal

    def spawnAlarm(self):
        self.alarmOn = True
        #converted to wav using Audacity from https://commons.wikimedia.org/wiki/File:Boreal_Owl.ogg
        self.wav = sa.WaveObject.from_wave_file("sounds/Boreal_Owl.wav")
        self.play = self.wav.play()

    def stopAlarm(self):
        if (self.alarmOn):
            self.play.stop()
            self.alarmOn = False

class ClockThread(Thread):
    def __init__(self, gui):
        Thread.__init__(self)
        self.gui = gui
 
    def run(self):
        self.runClock()

    def runClock(self):
        d = datetime.datetime.now()
        while self.gui.doCont():
            dstr = d.strftime("%B %d, %Y")
            t = d.strftime("%I:%M:%S %p")
            if (t[0] == '0'):
                 t = t[1:]
            self.checkAlarm(t)
            self.gui.configClock(dstr + "\n" + t)
            self.gui.configCalendar(d)
            d = datetime.datetime.now()
            time.sleep(1)

    def checkAlarm(self, t):
        if self.gui.getAlarmVal() == t:
            #this thread has the gui spawn an alarm thread 
            self.gui.spawnAlarm()

clock = ClockPlanner()
thr = ClockThread(clock)
thr.start()
clock.mainloop()
clock.stopAlarm()
thr.join()
